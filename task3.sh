#!/bin/bash
awk -F: '($3 >= 1000) {print}' /etc/passwd /etc/group > /tmp/GroupsAndUsers.txt

awk -F: '{print $1}' /tmp/GroupsAndUsers.txt > /tmp/usernames.txt

awk -F: '($1 ~ /^[\t ]*$/ && $3 >= 1000) || ($3 >= 1000) {print $1}' /tmp/GroupsAndUsers.txt > /tmp/groups.txt

user_count=$(wc -l < /tmp/usernames.txt)

group_count=$(wc -l < /tmp/groups.txt)

echo "Amount Of Users: $user_count" > /tmp/count.txt
echo "Amount Of Groups: $group_count" >> /tmp/count.txt

