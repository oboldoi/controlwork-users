#!/bin/bash 
sudo useradd -m -d /home/james -s /bin/bash -G developers james
sudo touch /home/james/hello.txt
sudo chgrp developers /home/james/hello.txt
sudo chmod g+rw /home/james/hello.txt
sudo useradd -m -d /home/robert -s /bin/bash -G developers robert
sudo touch /home/robert/hello.txt
sudo chgrp developers /home/robert/hello.txt
sudo chmod g+rw /home/robert/hello.txt
sudo usermod -aG developers james
sudo usermod -aG developers robert
